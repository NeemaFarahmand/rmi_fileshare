import java.rmi.Remote;
import java.rmi.RemoteException;

public interface FSInterface extends Remote {

	public String uploadFile(byte[] data, String serverPath) throws RemoteException;

	public byte[] downloadFile(String serverPath) throws RemoteException;

	public String listDirectory(String serverPath) throws RemoteException;

	public String makeDirectory(String serverPath) throws RemoteException;

	public String removeDirectory(String serverPath) throws RemoteException;

	public String removeFile(String serverPath) throws RemoteException;

	public void shutDown(String name) throws RemoteException;
	
}
