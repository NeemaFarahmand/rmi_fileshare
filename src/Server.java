import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;

/**
 * Server class that holds the main method to start the server. 
 * @author Neema
 *
 */
public class Server {
	
	public Server(){}
	
	public static void main(String[] args) {
		
		Server server = new Server();
		try {
			if (args[0].equals("start")) {
				int port = Integer.parseInt(args[1]);
				server.start(port);
			}
			else {
				System.out.println("The command you entered is invalid.\nError Code: 026");
			}
		}
		catch (ArrayIndexOutOfBoundsException | NumberFormatException e) {
			System.out.println("You must enter a valid port number.\nError Code: 027");
		}
	}

	public void start (int port) {
      try {
      	LocateRegistry.createRegistry(port);
         Naming.rebind("rmi://localhost:"+ port + "/Server", new FSImpl());
         System.out.println("Server is ready.");
      }
      catch (Exception e) {
         System.out.println("Server failed: " + e);
      }
	}
}
