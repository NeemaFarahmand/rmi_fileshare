import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;

public class FSImpl extends UnicastRemoteObject implements FSInterface  {

	private static final long serialVersionUID = -1848695857936956585L;

	public FSImpl() throws RemoteException {}

	@Override
	public String uploadFile(byte[] data, String serverPath) throws RemoteException {
		String completePath = Paths.get(".").toAbsolutePath().normalize().toString() + serverPath;
		try {
			FileOutputStream fos = new FileOutputStream(completePath);
			fos.write(data);
			fos.close();
		} 
		catch (FileNotFoundException e) {
			return "Unable to create file on server.\nError code: 014";
		} 
		catch (IOException e) {
			return "Unable to write data to file.\nError code: 015";
		}
		return "File uploaded sucessfully.";
	}

	@Override
	public byte[] downloadFile(String serverPath) throws RemoteException {
		String completePath = Paths.get(".").toAbsolutePath().normalize().toString() + serverPath;
		File file = new File(completePath);
		if (!file.exists()){
			return null;
		}
		Path fileToDownload = Paths.get(completePath);
		byte [] data = null;
		try {
			data = Files.readAllBytes(fileToDownload);
		} catch (IOException e) {
			return null;
		}
		return data;
	}

	@Override
	public String listDirectory(String serverPath) throws RemoteException {
		String completePath = Paths.get(".").toAbsolutePath().normalize().toString() + serverPath;
		File file = new File(completePath);
		if (file.isDirectory()){
			File[] listOfAll = file.listFiles();
			ArrayList<String> list = new ArrayList<>();
			list.add("Current Directory: " + file.getName());
			for (File f:listOfAll){
				if (!f.isHidden()){
					list.add(f.getName());
				}
			}
			StringBuilder sb = new StringBuilder();
			for (String s: list){
				sb.append(s + "\n");
			}
			return sb.toString();
		} else{
			return "Requested directory does not exist.\nError code: 016";
		}
	}

	@Override
	public String makeDirectory(String serverPath) throws RemoteException {
		String completePath = Paths.get(".").toAbsolutePath().normalize().toString() + serverPath;
		File file = new File(completePath);
		if (file.isDirectory()){
			return "Directory already exists.\nError code: 017";
		}else if (file.mkdir()){
			return "Directory succesfully created.";
		}else{
			return "Server failed to create directory.\nError code: 018";
		}
	}

	@Override
	public String removeDirectory(String serverPath) throws RemoteException {
		String completePath = Paths.get(".").toAbsolutePath().normalize().toString() + serverPath;
		File file = new File(completePath);
		if (file.exists()) {
			if (file.isDirectory()) {
				if (file.delete()) {
					return "Directory sucessfully removed.";
				} else {
					return "Directory not removed, make sure directory is empty.\nError code: 019";
				}
			} else {
				return "The path given is not a directory.\nError code: 020"; //could be a file 
			}
		} else {
			return "Directory does not exist.\nError code: 021";
		}
	}

	@Override
	public String removeFile(String serverPath) throws RemoteException {
		String completePath = Paths.get(".").toAbsolutePath().normalize().toString() + serverPath;
		File file = new File(completePath);
		if (file.exists()) {
			if (file.isFile()) {
				if (file.delete()) {
					return "File sucessfully removed.";
				} else {
					return "File not removed.\nError code: 022"; // unknown error
				}
			} else {
				return "The path given is not a file.\nError code: 023"; //could be directory 
			}
		} else {
			return "File does not exist.\nError code: 024";
		}
	}

	@Override
	public void shutDown(String name) throws RemoteException {
		System.out.println("Server shutting down.");
		try {
			Naming.unbind(name);
			UnicastRemoteObject.unexportObject(this, true);
		} 
		catch (MalformedURLException | NotBoundException e) {
			System.out.println("Server was unable to shutdown: " + e.getMessage());
			System.out.println("Error code: 025");
		} 
	}
}
