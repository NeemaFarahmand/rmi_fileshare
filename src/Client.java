import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.rmi.Naming;

public class Client {

	private static String SERVER;
	private static int PORT;
	private static String name;
	private FSInterface server;
	
	// testing
	
	public static void main(String args[]) {
		
		Client client = new Client();
		try {
			switch (args[0]) {
			case "upload":
				client.uploadFile(args[1], args[2]);
				break;
			case "download":
				client.downloadFile(args[1], args[2]);
				break;
			case "dir":
				client.listDirectory(args[1]);
				break;
			case "mkdir":
				client.makeDirectory(args[1]);
				break;
			case "rmdir":
				client.removeDirectory(args[1]);
				break;
			case "rm":
				client.removeFile(args[1]);
				break;
			case "shutdown":
				client.shutDown();
				break;
			default: System.out.println("The command you entered is invalid.\nError Code: 001");
			}
		}
		catch (ArrayIndexOutOfBoundsException e){
			System.out.println("The command you entered is invalid.\nError Code: 001");
		}
	}
	
	public void uploadFile(String pathOnClient, String pathOnServer) {
		setVariables();
		try {
			server = (FSInterface)Naming.lookup(name);
			String completePath = Paths.get(".").toAbsolutePath().normalize().toString() + pathOnClient;
			Path fileToUpload = Paths.get(completePath);
			byte[] data = Files.readAllBytes(fileToUpload);
	      System.out.println(server.uploadFile(data, pathOnServer));
		}
		catch (NoSuchFileException e) {
	   	System.out.println("File does not exist on client.\nError code: 002");
	   } 
		catch (Exception e) {
			System.out.println("Communication with server failed: " + e.getMessage());
			System.out.println("Error code: 003");
		}
	}

	public void downloadFile(String pathOnServer, String pathOnClient) {
		setVariables();
		try {
			server = (FSInterface)Naming.lookup(name);
			byte[] data = server.downloadFile(pathOnServer);
			if (data == null){
				System.out.println("Unable to download file from the server.\nError code: 004");
			}
			else {
				String completePath = Paths.get(".").toAbsolutePath().normalize().toString() + pathOnClient;
				try {
					FileOutputStream fos = new FileOutputStream(completePath);
					fos.write(data);
					fos.close();
					System.out.println("File downloaded successfully");
				} 
				catch (FileNotFoundException e) {
					System.out.println("Unable to create file on client.\nError code: 005"); // did not enter file name
				} 
				catch (IOException e) {
					System.out.println("Unable to write data to file.\nError code: 006");
				}
			}
		}
		catch (Exception e) {
			System.out.println("Communication with server failed: " + e.getMessage());
			System.out.println("Error code: 007");
		}
	}

	public void listDirectory(String serverPath) {
		setVariables();
		try {
	      server = (FSInterface)Naming.lookup(name);
	      System.out.println(server.listDirectory(serverPath));
	   }
	   catch (Exception e) {
	   	System.out.println("Communication with server failed: " + e.getMessage());
			System.out.println("Error code: 008");
	   }
	}

	public void makeDirectory(String newDirectoryPath) {
		setVariables();
      try {
         server = (FSInterface)Naming.lookup(name);
         System.out.println(server.makeDirectory(newDirectoryPath));
       } 
       catch (Exception e) {
      	 System.out.println("Communication with server failed: " + e.getMessage());
 			 System.out.println("Error code: 009");
       }
    }
	
	public void removeDirectory(String directoryToRemove) {
		setVariables();
      try {
         server = (FSInterface)Naming.lookup(name);
         System.out.println(server.removeDirectory(directoryToRemove));
      }
      catch (Exception e) {
      	System.out.println("Communication with server failed: " + e.getMessage());
			System.out.println("Error code: 010");
      }
	}
	
	public void removeFile(String fileToRemove) {
		setVariables();
		try {
			server = (FSInterface)Naming.lookup(name);
			System.out.println(server.removeFile(fileToRemove));
		}
      catch (Exception e) {
      	System.out.println("Communication with server failed: " + e.getMessage());
			System.out.println("Error code: 011");
      }
	}
	
	public void shutDown() {
		setVariables();
		try {
			server = (FSInterface)Naming.lookup(name);
			server.shutDown(name);
			System.out.println("Server has shut down.");
		}
		catch (Exception e) {
			System.out.println("Communication with server failed: " + e.getMessage());
			System.out.println("Error code: 012");
		}
	}
	
	private void setVariables() {
		try {
			String serverAndPort = System.getenv("PA2_SERVER");
			String[] var = serverAndPort.split(":");
			SERVER = var[0];
			PORT = Integer.parseInt(var[1]);
			name = "rmi://" + SERVER + ":" + PORT + "/Server";
		}
		catch (NullPointerException e){
			System.out.println("Enovironment variables not set.\nError code: 013");
		}
	}
}